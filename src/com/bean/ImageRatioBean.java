/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class ImageRatioBean {
    private String imageName;
    private double viewDimention;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public double getViewDimention() {
        return viewDimention;
    }

    public void setViewDimention(double viewDimention) {
        this.viewDimention = viewDimention;
    }
}
