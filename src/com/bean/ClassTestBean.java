/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class ClassTestBean {
    private int TestId;
    private int GroupId;
    private String Status;
    private String TestDate;
    private int TestTotalTime;
    private String TestName;
    private String AcademicYear;
    private String Class_Std;
    private String Divission;

    public String getAcademicYear() {
        return AcademicYear;
    }

    public void setAcademicYear(String AcademicYear) {
        this.AcademicYear = AcademicYear;
    }

    public String getClass_Std() {
        return Class_Std;
    }

    public void setClass_Std(String Class_Std) {
        this.Class_Std = Class_Std;
    }

    public String getDivission() {
        return Divission;
    }

    public void setDivission(String Divission) {
        this.Divission = Divission;
    }
    private String START_DATE;
    private String EXPIRE_DATE;

   
    

    public String getSTART_DATE() {
        return START_DATE;
    }

    public void setSTART_DATE(String START_DATE) {
        this.START_DATE = START_DATE;
    }

    public String getEXPIRE_DATE() {
        return EXPIRE_DATE;
    }

    public void setEXPIRE_DATE(String EXPIRE_DATE) {
        this.EXPIRE_DATE = EXPIRE_DATE;
    }
    
            
    public int getTestId() {
        return TestId;
    }

    public void setTestId(int TestId) {
        this.TestId = TestId;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int GroupId) {
        this.GroupId = GroupId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getTestDate() {
        return TestDate;
    }

    public void setTestDate(String TestDate) {
        this.TestDate = TestDate;
    }

    public int getTestTotalTime() {
        return TestTotalTime;
    }

    public void setTestTotalTime(int TestTotalTime) {
        this.TestTotalTime = TestTotalTime;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String TestName) {
        this.TestName = TestName;
    }
}
