/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

//import java.beans.Statement;
import server.DBConnection1;
import server.Server;
import java.sql.*;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author rcs
 */
public class CreateTableRegistration {
    
    public void createTable()
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            Statement s=conn.createStatement();
            s.execute("CREATE TABLE  Register(CD_Key VARCHAR(100) NOT NULL,PIN VARCHAR(100) NOT NULL,Motherboard_SerialNO VARCHAR(100) NOT NULL,Register_Time VARCHAR(1000) NOT NULL,PRIMARY KEY (CD_Key))");
            JOptionPane.showMessageDialog(null,"TableCreated");
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public void dropTable()
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            Statement s=conn.createStatement();
            s.execute("drop table Register");
            JOptionPane.showMessageDialog(null,"Table Droped");
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }        
    }
    public static void main(String s[])
    {
        //new CreateTableRegistration().dropTable();
        new CreateTableRegistration().createTable();
    }
}