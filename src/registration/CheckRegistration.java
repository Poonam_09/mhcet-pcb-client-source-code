/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

import server.DBConnection1;
import server.Server;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import ui.LoginStudentForm;
import ui.NewTestForm;

/**
 * * @author rcs
 */
public class CheckRegistration {
//    Statement st;
//    ResultSet rs;
    String PIN;
    String cdkey;
    String mis;
    NewTestForm ts;
    int rollNo;
    
    public CheckRegistration(){
    }

    public CheckRegistration(int rollNo) {
        this.rollNo=rollNo;
    }
    
    public void setTs(NewTestForm ts) {
        this.ts = ts;        
    }
    
    
    public void checkRegistration()
    {   
        String cpuid=CUIDGeneratorNew.uniqueCPUID();
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            Statement st=conn.createStatement();
            ResultSet rs=st.executeQuery("select * from Register");
            int c=0;
            while(rs.next())
            {
                cdkey=rs.getString(1);
                PIN=rs.getString(2);
                mis=rs.getString(3);
                if (cpuid.equals(mis)) {
                    c=1;
                    break;
                }
                c++;
            }
            if(c==0)
            {
                new RegistrationForm().setVisible(true);
            }
            else if(!mis.equals(CUIDGeneratorNew.uniqueCPUID()))
            {
                new RegistrationForm().setVisible(true);
            }
            else if(new PINChecker().checkPIN(PIN,cdkey)==0)
            {
                new RegistrationForm().setVisible(true);
            }
            else if(new PINChecker().checkPIN(PIN,cdkey)==1)
            {
                new LoginStudentForm().setVisible(true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }  
    }
    
   public void checkRegistration(boolean b,boolean b1)
    {
        String cpuid=CUIDGeneratorNew.uniqueCPUID();
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            Statement st=conn.createStatement();
            ResultSet rs=st.executeQuery("select * from Register");
            int c=0;
            while(rs.next())
            {
                cdkey=rs.getString(1);
                PIN=rs.getString(2);
                mis=rs.getString(3);
                if (cpuid.equals(mis)) {
                    c=1;
                    break;
                }
                c++;
            }
            if(c==0)
            {
                new RegistrationForm().setVisible(true);
            }
            else if(!mis.equals(CUIDGeneratorNew.uniqueCPUID()))
            {
                new RegistrationForm().setVisible(true);
            }
            else if(new PINChecker().checkPIN(PIN,cdkey)==0)
            {
                new RegistrationForm().setVisible(true);
            }
            else if(new PINChecker().checkPIN(PIN,cdkey)==1)
            {
                new NewTestForm(rollNo).setVisible(true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }  
    }
    public static  void main(String s[])
    {
        new CheckRegistration(5).checkRegistration();
    }
}