/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rcs
 */
public class HashCodeGenerator {
    public String calculateSecurityHash(String key,String algorithm) throws java.security.NoSuchAlgorithmException 
    {
        String hexMessageEncode = "";
        byte[] buffer = key.getBytes();
        java.security.MessageDigest messageDigest =java.security.MessageDigest.getInstance(algorithm);
        messageDigest.update(buffer);
        byte[] messageDigestBytes = messageDigest.digest();
        for (int index=0; index < messageDigestBytes.length ; index ++) {
        int countEncode = messageDigestBytes[index] & 0xff;
        if (Integer.toHexString(countEncode).length() == 1) hexMessageEncode = hexMessageEncode + "0";
        hexMessageEncode = hexMessageEncode + Integer.toHexString(countEncode);
        if( "MD5".equals(algorithm))
        {
            hexMessageEncode=hexMessageEncode+"0";
        }
        else if("MD2".equals(algorithm))
        {
            hexMessageEncode=hexMessageEncode+"1";
        }
        else if("SHA".equals(algorithm))
        {
            hexMessageEncode=hexMessageEncode+"2";
        }
        else if("SHA1".equals(algorithm))
        {
            hexMessageEncode=hexMessageEncode+"3";
        }
    }


        return hexMessageEncode;
    }
    public String pinHash(String key,String algorithmName) throws java.security.NoSuchAlgorithmException
    {
        String hexMessageEncode = "";
        byte[] buffer = key.getBytes();
        java.security.MessageDigest messageDigest =
        java.security.MessageDigest.getInstance(algorithmName);
        messageDigest.update(buffer);
        byte[] messageDigestBytes = messageDigest.digest();
        for (int index=0; index < messageDigestBytes.length ; index ++) {
         int countEncode = messageDigestBytes[index] & 0xff;
        if (Integer.toHexString(countEncode).length() == 1) hexMessageEncode = hexMessageEncode + "0";
        hexMessageEncode = hexMessageEncode + Integer.toHexString(countEncode);
        }

        return hexMessageEncode;
    }
    public static void main(String s[])
    {
        try {
            System.out.print(
            new HashCodeGenerator().calculateSecurityHash("rajnish","MD5"));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HashCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}