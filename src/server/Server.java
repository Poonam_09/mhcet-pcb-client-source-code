package server;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 007
 */
public class Server {
//   public static String serverIP="127.0.0.1";
    
    public static String getServerIP(){
        String serverIP="";
        Properties prop = new Properties(); 
    	try {
               //load a properties file
    		prop.load(new FileInputStream("config.properties")); 
               //get the property value and print it out
                serverIP=prop.getProperty("serverIP");
                
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
        return serverIP;
    }
    
    public static void setServerIP(String serverIP){
    	try {
    		Properties prop = new Properties();
                prop.setProperty("serverIP", serverIP);
    		prop.store(new FileOutputStream("config.properties"), null); 
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
    }
}
