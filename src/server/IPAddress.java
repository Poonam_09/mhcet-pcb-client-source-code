/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Avadhut
 */
public class IPAddress {
    public String getIPAddress(String prefix){
        try {
            Enumeration e=NetworkInterface.getNetworkInterfaces();
            
                while(e.hasMoreElements())
                {
                    NetworkInterface n=(NetworkInterface) e.nextElement();
                    Enumeration ee = n.getInetAddresses();
                    
                    while(ee.hasMoreElements())
                    {
                        Object object = ee.nextElement();
                        if(object instanceof Inet4Address){
                            Inet4Address i= (Inet4Address) object;
                            String hostAddress = i.getHostAddress();
                            String clientName=i.getHostName();                                              
                            if(hostAddress.startsWith(prefix)){
                                System.out.println(hostAddress);
                                System.out.println("***"+clientName);
                                return clientName;
                            }                           
                        }
                    }
                }
                
        } catch (SocketException ex) {
            Logger.getLogger(IPAddress.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
    
    public static void main(String[] s){
        new IPAddress().getIPAddress("192.168.2");
    }
}
