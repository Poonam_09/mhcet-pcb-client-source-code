/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import javax.swing.JLabel; 
  
class MyModel extends javax.swing.table.DefaultTableModel{ 
  
    Object[][] row = {}; 
  
    Object[] col = {"Question Number", "User Answer", "Result"}; 
  
    
    public MyModel (){ 
  
    //Adding columns 
        for(Object c: col) 
            this.addColumn(c); 
  
    //Adding rows 
        for(Object[] r: row) 
            addRow(r); 
  
    } 
  
    @Override
  
    public Class getColumnClass(int columnIndex) { 
        if(columnIndex == 1)return getValueAt(0, columnIndex).getClass(); 
        else if(columnIndex == 2)return getValueAt(0, columnIndex).getClass(); 
        else return super.getColumnClass(columnIndex); 
  
    } 
  
}
