package ui;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 007
 */
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import server.DBConnection1;

public class EmployeeShow {

    public ImageIcon RetriveImage(String path) {
        ImageIcon image = null;
        try {
            Connection con =new DBConnection1().getClientConnection1(server.Server.getServerIP());;
//            String halfurl = new DBConnection1().getURL(server.Server.getServerIP());
//            con = DriverManager.getConnection(halfurl);
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("select ImageObject from AllImages where ImageName = '" + path + "'");
            if (rs.next()) {
                Blob photo = rs.getBlob(1);
                ObjectInputStream ois = null;
                ois = new ObjectInputStream(photo.getBinaryStream());
                image = (ImageIcon) ois.readObject();
            }
            s.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EmployeeShow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeShow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeShow.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;
    }
}