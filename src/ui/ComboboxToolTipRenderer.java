/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
public class ComboboxToolTipRenderer extends DefaultListCellRenderer {
    ArrayList<String> tooltips;

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                        int index, boolean isSelected, boolean cellHasFocus) {

        JComponent comp = (JComponent) super.getListCellRendererComponent(list,
                value, index, isSelected, cellHasFocus);

        if (-1 < index && null != value && null != tooltips) {
                    list.setToolTipText(tooltips.get(index));
                }
        return comp;
    }

    public void setTooltips(ArrayList<String> tooltips) {
        this.tooltips = tooltips;
    }
}
