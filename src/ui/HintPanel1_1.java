/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * QuestionPanel.java
 *
 * Created on Nov 5, 2012, 2:28:02 PM
 */
package ui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.scilab.forge.jlatexmath.*;
import NEET.question.QuestionBean;

/**
 *
 * @author Administrator
 */
public class HintPanel1_1 extends javax.swing.JPanel {

    /** Creates new form QuestionPanel */
    String start,end,qImagePath="";
    QuestionBean currentQuestion;
    String hint;
    
    public HintPanel1_1()
    {
        initComponents();  
    }
    
    public HintPanel1_1(QuestionBean questionBean, int index) {
        initComponents(); 
        
        start = "\\begin{array}{l}";
	end = "\\end{array}";
        
        lblHint.setVisible(false);
//        rdoOptionA.setEnabled(false);
//        rdoOptionB.setEnabled(false);
//        rdoOptionC.setEnabled(false);
//        rdoOptionD.setEnabled(false);
        jLabel1.setVisible(false);
        
        setQuestionOnPanel(questionBean, index);
//        rdoOptionA.setActionCommand("A");
//        rdoOptionB.setActionCommand("B");
//        rdoOptionC.setActionCommand("C");
//        rdoOptionD.setActionCommand("D");        
       
//        buttonGroup1.add(rdoOptionA);        
//        buttonGroup1.add(rdoOptionB);
//        buttonGroup1.add(rdoOptionC);
//        buttonGroup1.add(rdoOptionD);
    }
    
    public void showOptions()
    {
//                lblA.setVisible(true);
//                lblOptionA.setVisible(true);
//                lblB.setVisible(true);
//                lblOptionB.setVisible(true);
//                lblC.setVisible(true);
//                lblOptionC.setVisible(true);
//                lblD.setVisible(true);
//                lblOptionD.setVisible(true);
//                lblOptionAsImage.setVisible(false);                
    }
    public void showHint()
    {
        
    }
    public void hideOptions()
    {
//                lblA.setVisible(false);
//                lblOptionA.setVisible(false);
//                lblB.setVisible(false);
//                lblOptionB.setVisible(false);
//                lblC.setVisible(false);
//                lblOptionC.setVisible(false);
//                lblD.setVisible(false);
//                lblOptionD.setVisible(false);
//                lblOptionAsImage.setVisible(true);                
    }
    
    public void setLableText(JLabel l,String str)
    {
        try
        {
            if(str.equals(""))
            {
                
            }
            else
            {
                l.setText("");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;                
                JLabel jl;
                str=start+str+end;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0,0,0,0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);   
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.getMessage()); 
        }
    }
    
    public void showQuestionImage()
    {         
//                lblQuestionAsImage.setVisible(true);                
    }
    public void hideQuestionImage()
    {                
//                lblQuestionAsImage.setVisible(false);                
    }
    public void loadImage(String path,JLabel lbl)
    {   
        BufferedImage image;
        try{            
                File file=new File(path);
                image=ImageIO.read(file);
                ImageIcon icon;
                float width=image.getWidth();
                float height=image.getHeight();
                width=(float) (width*0.3);
                height=(float) (height*0.3);
                //Determine how the image has to be scaled if it is large:
                Image thumb = image.getScaledInstance((int)width,(int)height, Image.SCALE_AREA_AVERAGING);
                icon=new ImageIcon(thumb);
                
                lbl.setIcon(icon);
                lbl.setText("");               
            }        
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    
    public void setQuestionOnPanel(QuestionBean question,int index)
    {
            currentQuestion=question;
            jLabel1.setVisible(false);
            hint=currentQuestion.getHint();
            loadImage(question.getQuestionImagePath(), lblHintImage);
            qImagePath=currentQuestion.getQuestionImagePath();
             if(hint!=null)
            {
                if(question.getHint()=="" || question.getHint()==null)
                {
                    lblHint.setVisible(false);
                    lblHint.setText("");
                    jLabel1.setVisible(false);
                    
                }
                else
                {
                    jLabel1.setVisible(true);
                    lblHint.setVisible(true);
                    setLableText(lblHint,question.getHint());
                }
            }
//            setLableText(lblQuestion,question.getQuestion());
            if(!question.isIsQuestionAsImage())
            {
                hideQuestionImage();
            }
            else
            {
                showQuestionImage();                
//                loadImage(question.getQuestionImagePath(), lblQuestionAsImage);                
            }
            if(!question.isIsOptionAsImage())
            {
                showOptions();                
//                setLableText(lblOptionA,question.getA());            
//                setLableText(lblOptionB,question.getB());
//                setLableText(lblOptionC,question.getC());
//                setLableText(lblOptionD,question.getD());
            }
            else
            {
                hideOptions();   
                //loadImage(question.getOptionImagePath(), lblOptionAsImage);
            }            
            
//            String userAnswer=question.getUserAnswer();
//            if(userAnswer.equals("UnAttempted"))
//            {                
//                lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/unAttempted.png")));
//                lblCorrectAnswer.setText(currentQuestion.getAnswer());
//            }
//            else
//            {
//                if(userAnswer.equals("A"))
//                {
//                    rdoOptionA.setSelected(true);
//                }
//                else if(userAnswer.equals("B"))
//                {
//                    rdoOptionB.setSelected(true);
//                }
//                else if(userAnswer.equals("C"))
//                {
//                    rdoOptionC.setSelected(true);
//                }
//                if(userAnswer.equals("D"))
//                {
//                    rdoOptionD.setSelected(true);
//                }      
//                
//                if(userAnswer.equals(currentQuestion.getAnswer()))
//                {
//                    lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png")));
//                    lblCorrect.setVisible(false);
//                    lblCorrectAnswer.setVisible(false);
//                    
//                }
//                else
//                {
//                    lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wrong.png")));
//                    lblCorrectAnswer.setText(currentQuestion.getAnswer());
//                }
//            }            
//            lblQuestionNo.setText("Q. "+index);
   }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        lblHint = new javax.swing.JLabel();
        lblHintImage = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setAutoscrolls(true);
        setPreferredSize(new java.awt.Dimension(765, 500));

        jPanel1.setName("jPanel1"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabel1.setText("Hint");
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel1.add(jLabel1);

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanel2.setName("jPanel2"); // NOI18N

        lblHint.setForeground(new java.awt.Color(255, 0, 0));
        lblHint.setText(".");
        lblHint.setName("lblHint"); // NOI18N
        jPanel2.add(lblHint);

        lblHintImage.setText(".");
        lblHintImage.setName("lblHintImage"); // NOI18N
        lblHintImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblHintImageMouseClicked(evt);
            }
        });
        jPanel2.add(lblHintImage);

        jScrollPane1.setViewportView(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 785, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 444, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void lblHintImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblHintImageMouseClicked
            switch(evt.getModifiers()){
            case InputEvent.BUTTON1_MASK:{
                 new ZoomImage_1(qImagePath).setVisible(true);
            }            
        }
}//GEN-LAST:event_lblHintImageMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblHint;
    private javax.swing.JLabel lblHintImage;
    // End of variables declaration//GEN-END:variables
}