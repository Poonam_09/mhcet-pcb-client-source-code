/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JEE.test;

import server.DBConnection1;
import server.Server;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SaveAllTests {
    
    public void saveTestBean(TestBean testBean,String testName,int rollNo){
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try
        {  
            int savedId=0;
            Statement s=conn.createStatement();            
            ResultSet rs = s.executeQuery("Select max(savedId) from Saved_All_Test");
                if(rs.next())
                {
                        savedId=rs.getInt(1);
                }
            savedId++;
            
            String sql="insert into Saved_All_Test values(?,?,?,?,?,?)";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, savedId);
            ps.setInt(2, rollNo);
            ps.setInt(3, testBean.getTestId());
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));           
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            ObjectOutputStream objOutStream = new ObjectOutputStream(byteOutStream);
            objOutStream.writeObject(testBean);
            objOutStream.flush();
            objOutStream.close();
            byteOutStream.close();
            ps.setBytes(5, byteOutStream.toByteArray());
            ps.setString(6, testName);
            ps.executeUpdate();
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public ArrayList<Integer> getTestId(int rollNo){
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<Integer> ids=new ArrayList<Integer>();
        try{
            String sql="select * from Saved_All_Test where rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                int s= rs.getInt(1);
                ids.add(s);
            }
            return ids;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ids;
    }
    
    public TestBean retrieveTestBean(int savedId){
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try
        {  
            String sql="select * from Saved_All_Test where savedId=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, savedId);
            ResultSet rs=ps.executeQuery();
            if(rs.next())
            {
                byte[] data = rs.getBytes(5) ;
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                if (obj instanceof TestBean) {
                    TestBean p = (TestBean)obj;
                return p;
                }
            }
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public ArrayList<TestBean> retrieveAllTestBeans(int rollNo){
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<TestBean> testBeans=new ArrayList<TestBean>();
        try
        {  
            String sql="select * from Saved_All_Test where rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                byte[] data = rs.getBytes(5) ;
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                if (obj instanceof TestBean) {
                    TestBean p = (TestBean)obj;
                    testBeans.add(p);
                }
            }
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testBeans;
    }

    public ArrayList<String> getNames(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<String> testDate=new ArrayList<String>();
        try{
            String sql="select * from Saved_All_Test where rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                String s= rs.getString(6);
                testDate.add(s);
            }
            return testDate;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testDate;
    }
    
    public ArrayList<String> getDate(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<String> testDate=new ArrayList<String>();
        try{
            String sql="select * from Saved_All_Test where rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                String s = rs.getString(4);
                testDate.add(s);
            }
            return testDate;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testDate;
    }
    
    public Integer isTestPresent(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try{
            String sql="select * from Saved_All_Test where rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            if(rs.next())
            {
                return 1;
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
    
}