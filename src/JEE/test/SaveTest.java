package JEE.test;

import server.DBConnection1;
import server.Server;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class SaveTest {
    public void saveTestBean(TestBean testBean,int rollNo){
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try
        {  
            int testId=0;
            Statement s=conn.createStatement();
            ResultSet rs = s.executeQuery("Select max(id) from Saved_Test_Info");
                if(rs.next())
                {
                        testId=rs.getInt(1);
                }
            testId++;
            
            String sql="insert into Saved_Test_Info values(?,?,?,?,?)";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            ps.setInt(2, rollNo);
            ps.setInt(3, 1);//TestBean = 1
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            ObjectOutputStream objOutStream = new ObjectOutputStream(byteOutStream);
            objOutStream.writeObject(testBean);
            objOutStream.flush();
            objOutStream.close();
            byteOutStream.close();
            ps.setBytes(5, byteOutStream.toByteArray());
            int i=ps.executeUpdate();
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public ArrayList<Integer> getTestId(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<Integer> ids=new ArrayList<Integer>();
        try{
            String sql="select * from Saved_Test_Info where testType=1 and rollNo=";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                int s= rs.getInt(1);
                ids.add(s);
            }
            return ids;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ids;
    }
    
    public ArrayList<TestBean> retrieveTestBean(int rollNo){
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<TestBean> testBeans=new ArrayList<TestBean>();
        try
        {  
            String sql="select * from Saved_Test_Info where testType=1 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                TestBean p = new TestBean();
                byte[] data = rs.getBytes(5) ;
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                if (obj instanceof TestBean) {
                    p = (TestBean)obj;
                }
                testBeans.add(p);
            }
            return testBeans;
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return null;
    }
    
    public void delTest(int testId)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try
        {
            String sql="delete from Saved_Test_Info where id=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i=ps.executeUpdate();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public ArrayList<String> getDate(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<String> testDate=new ArrayList<String>();
        try{
            String sql="select * from Saved_Test_Info where testType=1 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                String s= rs.getString(4);
                testDate.add(s);
            }
            return testDate;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return testDate;
    }
    
    public Integer isTestPresent(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try{
            String sql="select * from Saved_Test_Info where testType=1 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            if(rs.next())
            {
                return 1;
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return 0;
    }
    
    public static void main(String[]args)
    {
    }    
}