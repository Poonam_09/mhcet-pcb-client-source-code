/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.unitTest;
import java.io.*;
/**
 *
 * @author Avadhut
 */
public class SerializePractice {
    

    public File[] finder( String dirName){
    	File dir = new File(dirName);

    	return dir.listFiles(new FilenameFilter() { 
    	         public boolean accept(File dir, String filename)
    	              { return filename.endsWith(".ser"); }
    	} );
    }
    
    public static void serialize(UnitTestBean unitTestBean)
    {
        try
        {
            FileOutputStream fileOut = new FileOutputStream("unittest.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(unitTestBean);
            out.close();
            fileOut.close();
        }
        catch(IOException i)
        {
            i.printStackTrace();
        }
    }
    
    public static UnitTestBean deSerialize()
    {
        try
        {
            FileInputStream fileIn = new FileInputStream("unittest.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            UnitTestBean testBean = (UnitTestBean) in.readObject();
            in.close();
            fileIn.close();
            File f1 = new File("unittest.ser");
            boolean success = f1.delete();
            if (!success){
                //System.out.println("Deletion failed."); 
            }else{
                //System.out.println("File deleted.");
            }
            return testBean;
        }
        catch(IOException i)
        {
            i.printStackTrace();            
        }
        catch(ClassNotFoundException c)
        {
            //System.out.println("TestBean class not found.");
            c.printStackTrace();            
        }
        return null;        
    }
}